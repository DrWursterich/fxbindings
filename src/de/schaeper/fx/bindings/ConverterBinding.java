package de.schaeper.fx.bindings;

import java.lang.ref.WeakReference;
import java.util.function.Function;

import javafx.beans.WeakListener;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;


public class ConverterBinding<T, N> implements ChangeListener<T>, WeakListener {
	private WeakReference<Property<T>> fromPropertyReference;
	private WeakReference<Property<N>> toPropertyReference;
	private Function<T, N> converter;

	private boolean updating = false;

	public static <T, N> ConverterBinding<T, N> bind(
			final Property<T> fromProperty,
			final Property<N> toProperty,
			final Function<T, N> converter) {
		final ConverterBinding<T, N> binding = new ConverterBinding<>(
				fromProperty,
				toProperty,
				converter);
		binding.applyProperty(fromProperty, toProperty, converter);
		fromProperty.addListener(binding);
		return binding;
	}

	public ConverterBinding(
			final Property<T> fromProperty,
			final Property<N> toProperty,
			final Function<T, N> converter) {
		if (fromProperty == null || toProperty == null) {
			throw new NullPointerException(
					"Both properties must be specified.");
		}
		if (fromProperty == toProperty) {
			throw new IllegalArgumentException(
					"Cannot bind property to itself.");
		}
		this.fromPropertyReference = new WeakReference<>(fromProperty);
		this.toPropertyReference = new WeakReference<>(toProperty);
		this.converter = converter;
	}

	@Override
	public boolean wasGarbageCollected() {
		return this.fromPropertyReference == null
				|| this.toPropertyReference == null;
	}

	@Override
	public void changed(
			final ObservableValue<? extends T> sourceProperty,
			final T oldValue,
			final T newValue) {
		if (this.updating) {
			return;
		}
		final Property<T> fromProperty = this.fromPropertyReference.get();
		if (fromProperty == null) {
			return;
		}
		final Property<N> toProperty = this.toPropertyReference.get();
		if (toProperty == null) {
			fromProperty.removeListener(this);
			return;
		}
		final N oldToPropertyValue = toProperty.getValue();
		try {
			this.updating = true;
			this.applyProperty(fromProperty, toProperty, this.converter);
		} catch (final RuntimeException exception) {
			try {
				toProperty.setValue(oldToPropertyValue);
			} catch (final Exception revertException) {
				revertException.addSuppressed(exception);
				fromProperty.unbind();
				throw new RuntimeException(
						"Binding failed together with an attempt to restore "
							+ "the source property to the previous value."
							+ " Removing the Binding from the property "
							+ fromPropertyReference,
						revertException);
			}
			throw new RuntimeException(
					"Binding failed, setting to the previous value",
					exception);
		} finally {
			this.updating = false;
		}
	}

	private <A, B> void applyProperty(
			final Property<A> fromProperty,
			final Property<B> toProperty,
			final Function<A, B> converter) {
		try {
			final A fromValue = fromProperty.getValue();
			toProperty.setValue(
					fromValue != null
						? converter.apply(fromValue)
						: null);
		} catch (final Exception exception) {
			toProperty.setValue(null);
		}
	}
}
