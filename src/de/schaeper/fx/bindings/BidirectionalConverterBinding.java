package de.schaeper.fx.bindings;

import java.lang.ref.WeakReference;
import java.util.function.Function;

import de.schaeper.util.conditionals.TwoWayConditional;
import javafx.beans.WeakListener;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class BidirectionalConverterBinding<T, N>
		implements ChangeListener<Object>, WeakListener {
	private WeakReference<Property<T>> fromPropertyReference;
	private WeakReference<Property<N>> toPropertyReference;
	private Function<T, N> fromConverter;
	private Function<N, T> toConverter;

	private boolean updating = false;

	public static <T, N> BidirectionalConverterBinding<T, N> bind(
			final Property<T> fromProperty,
			final Property<N> toProperty,
			final Function<T, N> fromConverter,
			final Function<N, T> toConverter) {
		final BidirectionalConverterBinding<T, N> binding
				= new BidirectionalConverterBinding<>(
					fromProperty,
					toProperty,
					fromConverter,
					toConverter);
		binding.applyProperty(toProperty, fromProperty, toConverter);
		fromProperty.addListener(binding);
		toProperty.addListener(binding);
		return binding;
	}

	public BidirectionalConverterBinding(
			final Property<T> fromProperty,
			final Property<N> toProperty,
			final Function<T, N> fromConverter,
			final Function<N, T> toConverter) {
		if (fromProperty == null
				|| toProperty == null) {
			throw new NullPointerException(
					"Both properties must be specified.");
		}
		if (fromProperty == toProperty) {
			throw new IllegalArgumentException(
					"Cannot bin property to itself.");
		}
		this.fromPropertyReference = new WeakReference<>(fromProperty);
		this.toPropertyReference = new WeakReference<>(toProperty);
		this.fromConverter = fromConverter;
		this.toConverter = toConverter;
	}

	@Override
	public boolean wasGarbageCollected() {
		return this.fromPropertyReference == null
				|| this.toPropertyReference == null;
	}

	@Override
	public void changed(
			final ObservableValue<? extends Object> sourceProperty,
			final Object oldValue,
			final Object newValue) {
		if (this.updating) {
			return;
		}
		final Property<T> fromProperty = this.fromPropertyReference.get();
		final Property<N> toProperty = this.toPropertyReference.get();
		TwoWayConditional.conditions(fromProperty == null, toProperty == null)
				.runIfOnlyFirst(() -> toProperty.removeListener(this))
				.runIfOnlySecond(() -> fromProperty.removeListener(this))
				.runIfNeither(() -> {
					this.updating = true;
					try {
						if (fromProperty == sourceProperty) {
							this.applyProperty(
								fromProperty,
								toProperty,
								this.fromConverter);
						} else {
							this.applyProperty(
								toProperty,
								fromProperty,
								this.toConverter);
						}
					} finally {
						this.updating = false;
					}
				});
	}

	private <A, B> void applyProperty(
			final Property<A> fromProperty,
			final Property<B> toProperty,
			final Function<A, B> converter) {
		try {
			final A fromValue = fromProperty.getValue();
			toProperty.setValue(
					fromValue != null
						? converter.apply(fromValue)
						: null);
		} catch (final Exception exception) {
			toProperty.setValue(null);
		}
	}
}
