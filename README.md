# FxBindings

FxBindings is a small utility library, holding functions to ease Bindings between Properties from different Classes.

## Installing

Add the following lines to your `jam.ini`:
```ini
[fxbindings]
url=https://gitlab.com/DrWursterich/fxbindings
version=1.0.0
```

You can then run `jam build` as usual.
For more details see [jam](https://gitlab.com/DrWursterich/jam).

### Manually

You will also have to install the Dependencies manually, namely:
* [Conditionals](https://gitlab.com/DrWursterich/conditionals)

```bash
git clone https://gitlab.com/DrWursterich/conditionals.git
git clone https://gitlab.com/DrWursterich/fxbindings.git
cd fxbindings
javac $(find src -name *.java) --module-path /path/to/javafx-sdk --add-modules javafx.base -cp src:../conditionals/src
jar -cvf fxbindings.jar src
mv fxbindings.jar /my/project/lib/
```

## Examples

### ConverterBindings

```java
final ObjectProperty<FirstClass> firstClassProperty;
final ObjectProperty<SecondClass> secondClassProperty;

ConverterBinding.bind(
		firstClassProperty,
		secondClassProperty,
		firstClass -> new SecondClass(firstClass.getName());

BidirectionalConverterBinding.bind(
		firstClassProperty,
		secondClassProperty,
		firstClass -> new SecondClass(firstClass.getName()),
		secondClass -> new FirstClass(secondClass.getName());

```

